title: about
---
### 自我介紹
大家好，我是Greddy，是一個軟體工程師。

從工作至今已約8年，一直在金融相關產業深耕。
建置過的系統從銀行的財富管理系統至P2P借貸系統，後來轉為專注在金流服務系統(信用卡收單、第三方支付、聚合支付)。在金融相關領域已有相當程度的了解且深感興趣。

目前工作已不侷限在開發上，產品規劃、系統分析與專案管理亦是我主要工作。
我喜歡解決問題，不管是業務邏輯上的釐清或是系統的開發，都是我在工作上獲取成就感的來源。

### 技術領域
- 後端語言
`Java`, `Python`, `Scala`, `Node.js`

- 前端語言
`AngularJS 2`, `Vue.js`, `jQuery`

- 資料庫
`Mysql`, `Oracle`, `MS SQL`

- Other
`Docker`, `Jenkins`, `Ansible`, `AWS`, `K8S`, `Heroku`

### 進行中專案
- Fin Data
Fin Data 是一個提供財經相關資料的網站。資料搜集是我的第一步，下一步想要進行量化投資與理財機器人等嘗試。目前架設在AWS Server上。
測試版本：http://18.139.84.41/

- 柯柯報價
透過Line Bot設定想要關注的股票和需要提醒的價格，除了可以查詢前一日的收盤價，另外當每日收盤時觸碰到設定之價格也會進行通知。
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTHrLMdng2YCvP0H4RKZYMCuNL5VUzERuwsHEov9sZu9NoFphyh9bTrHKKv_NE8tFhxnFgF5rXUka6x/embed?start=true&loop=true&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

- Combo Pay
聚合支付的簡單版本，提供商家快速串接大陸的第三方支付，目前包含支付寶與雲閃付。
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTjZFsgPUTk-o3W_bh3KA_XGOkmJnE0tM80-AsFQcpaa1rjQRB9z2K1W7ZZVjJTRy5nV6LsfZzKDYgS/embed?start=true&loop=true&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

- Playground
遊戲核心系統，如:龍虎、百家樂、骰寶...等遊戲。預計接下來加入運動彩、六合彩...等彩票類型遊戲。
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSNexfBnE7MWH8q55CMjbDhayW_5gp28aFSzXKWKGbgloYqmUF1M3-zpkJKeMn-d5h5u2PmP5aWPYCW/embed?start=true&loop=true&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### 網站聲明
該網站用於做為個人的技術筆記，文章中皆有轉載聲明。
文章皆秉持著幾個原則：
- 參考、轉載、複製⋯等等皆會註明轉載聲明。
- 加入個人一些思考、範例、實作、貼圖。
- 加入專案開發或是問題的實際解決方案。

### 感謝
若是您有任何想法、建議、問題，都歡迎透過郵件與我聯繫。

### Donate
謝謝您的支持與鼓勵
<iframe src="/html/donate.html" style="overflow-x:hidden;overflow-y:hidden; border:0xp none #fff; min-height:240px; width:100%;"  frameborder="0" scrolling="no" allowtransparency="true"></iframe>