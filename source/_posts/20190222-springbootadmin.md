---
title: Spring Boot Admin 微服務監控系統
date: 2019-02-22 11:35:21
categories:
- Java
tags: 
- spring boot admin
- devops
- spring boot
- devops
---
當所有的系統都要跟分散式或微服務掛上關係時，我們又需要如何在 production 中管理我們的 micro service 呢？今天想跟大家介紹一個微服務的監控系統[Spring Boot Admin](https://github.com/codecentric/spring-boot-admin)，由於都已經整合到 spring boot 中了，因此添加方式也非常的簡單，請大家跟著我的步驟來吧！

## Quick Start
今天我們使用之前寫的專案 [cpay-core_service](https://gitlab.com/cpay2019/cpay-core_service)、[cpay-payment_service](https://gitlab.com/cpay2019/cpay-payment_service)、[cpay-admin_server](https://gitlab.com/cpay2019/cpay-admin_server)當作範例。

### 新建 Admin Server 專案
- 首先我們需要先新建一個 Admin Server 的專案，並在 `pom.xml` 中加入 spring-boot-admin-starter-server 的 dependency，可參考[cpay-admin_server](https://gitlab.com/cpay2019/cpay-admin_server)

```
<dependency>
    <groupId>de.codecentric</groupId>
    <artifactId>spring-boot-admin-starter-server</artifactId>
    <version>2.0.1</version>
</dependency>
```

完整的 `pom.xml` 請參考如下
<script src="https://gitlab.com/cpay2019/cpay-admin_server/snippets/1828543.js"></script>

- 建立 Main Class：`CpayAdminApplicatioin.java`

```
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class CpayAdminApplicatioin {

    public static void main(String[] args) {
        SpringApplication.run(CpayAdminApplicatioin.class, args);
    }
}
```

- 調整啟動 port：`application.yml`

```
server.port: 8070
spring.application.name: admin-server
```

- 執行 Main Class，執行[localhost:8070](http://localhost:8070)

![](/image/20190222-springbootadmin/springbootadmin-01.jpg)

### 添加需要監控的服務
這邊將以[cpay-core_service](https://gitlab.com/cpay2019/cpay-core_service)為例。

- 在 core_service 的 `pom.xml`，增加 dependency

```
<dependency>
    <groupId>de.codecentric</groupId>
    <artifactId>spring-boot-admin-starter-client</artifactId>
    <version>2.0.1</version>
</dependency>
```

- 修改 `application.yml`

```
spring.boot.admin.client.url: "http://localhost:8070"
management.endpoints.web.exposure.include: "*"
```

- 需在 CpayWebSecurityConfigurerAdapter 增加需 ignore 的 path

```
@Override
public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/actuator/**");
}
```

- 啟動 core_service

![](/image/20190222-springbootadmin/springbootadmin-02.jpg)

![](/image/20190222-springbootadmin/springbootadmin-03.jpg)


### 補充說明
這寫此文章的時候，已經推出了 `2.1.3` 版本，但測試時卻出現錯誤，參考如下：
```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.1.3.RELEASE</version>
    <relativePath /> <!-- lookup parent from repository -->
</parent>

<dependencies>
    <dependency>
        <groupId>de.codecentric</groupId>
        <artifactId>spring-boot-admin-starter-server</artifactId>
        <version>2.1.3</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```
錯誤訊息：
>java.lang.IllegalStateException: Calling [asyncError()] is not valid for a request with Async state [MUST_DISPATCH]
	at org.apache.coyote.AsyncStateMachine.asyncError(AsyncStateMachine.java:440)
	at org.apache.coyote.AbstractProcessor.action(AbstractProcessor.java:512)
	at org.apache.coyote.Request.action(Request.java:430)
	at org.apache.catalina.core.AsyncContextImpl.setErrorState(AsyncContextImpl.java:396)
	at org.apache.catalina.connector.CoyoteAdapter.asyncDispatch(CoyoteAdapter.java:239)
	at org.apache.coyote.AbstractProcessor.dispatch(AbstractProcessor.java:241)
	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:53)
	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:834)
	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1415)
	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
	at java.lang.Thread.run(Thread.java:748)

主要原因應該是可[參考](https://github.com/spring-projects/spring-boot/issues/15057)該文章。

## Reference
[Spring Boot Admin](https://github.com/codecentric/spring-boot-admin)