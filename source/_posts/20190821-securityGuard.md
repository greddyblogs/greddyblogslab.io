---
title: 股票筆記 - 保全業分析
date: 2019-08-21 23:44:46
categories:
- 股票
tags: 
- 保全業股
- 投資理財
- 股票
---
今天想要跟大家分享一下台灣的保全業股，分別是`(9917)中保` 與 `(9925)新保`。

目前 `中保` 的市佔率高達 6 成，	`新保` 的市佔率為 3 成。

因保全公司會有自己的線路、監視器...等機器設備，客戶更換保全公司相對付出的成本代價較高，因此也具有`高轉換成本`的特性。

## 財務分析
> 下方皆為合併報表 - 單季

### 毛利率比較
{% echarts 400 '100%' %}
{
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['(9917)中保','(9925)新保']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['2015','2016','2017','2018', '2019/Q1', '2019/Q2']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'(9917)中保',
            type:'line',
            data:[36.01, 36.96, 35.61, 36.64, 36.48, 36.66]
        },
        {
            name:'(9925)新保',
            type:'line',
            data:[36.8, 35.13, 33.81, 32.49, 32.56, 31.93]
        }
    ]
}
{% endecharts %}
{% raw %}
<br/>
{% endraw %}
|年度/季度	|2019/Q2|2019/Q1|2018	|2017	|2016	|2015	|
|:---:		|---:	|---:	|---:	|---:	|---:	|---:	|
|(9917)中保	|36.66	|36.48	|36.64	|35.61	|36.96	|36.01	|
|(9925)新保	|31.93	|32.56	|32.49	|33.81	|35.13	|36.8	|

### 營利率比較
{% echarts 400 '100%' %}
{
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['(9917)中保','(9925)新保']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['2015','2016','2017','2018', '2019/Q1', '2019/Q2']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'(9917)中保',
            type:'line',
            data:[18.14, 19.5, 17.02, 19.12, 19.58, 19.17]
        },
        {
            name:'(9925)新保',
            type:'line',
            data:[14.54, 12.95, 11.5, 10.4, 9.93, 9.61]
        }
    ]
}
{% endecharts %}
{% raw %}
<br/>
{% endraw %}
|年度/季度	|2019/Q2|2019/Q1|2018	|2017	|2016	|2015	|
|:---:		|---:	|---:	|---:	|---:	|---:	|---:	|
|(9917)中保	|19.17	|19.58	|19.12	|17.02	|19.5	|18.14	|
|(9925)新保	|9.61	|9.93	|10.4	|11.5	|12.95	|14.54	|

### 淨利率比較
{% echarts 400 '100%' %}
{
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['(9917)中保','(9925)新保']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['2015','2016','2017','2018', '2019/Q1', '2019/Q2']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'(9917)中保',
            type:'line',
            data:[15.84, 4.77, 17.12, 15.67, 16.19, 16.33]
        },
        {
            name:'(9925)新保',
            type:'line',
            data:[16.05, 14.05, 14.22, 12.81, 7.98, 15.04]
        }
    ]
}
{% endecharts %}
{% raw %}
<br/>
{% endraw %}
|年度/季度	|2019/Q2|2019/Q1|2018	|2017	|2016	|2015	|
|:---:		|---:	|---:	|---:	|---:	|---:	|---:	|
|(9917)中保	|16.33	|16.19	|15.67	|17.12	|4.77	|15.84	|
|(9925)新保	|15.04	|7.98	|12.81	|14.22	|14.05	|16.05	|
{% raw %}
<br/>
{% endraw %}
> `中保` 在 2016 年獲利下降，主要是因為轉投資的復興航空下市認列虧損。

### 殖利率統計
- (9917)中保

|年度 	|股價最高	|股價最低	|股價年均	|現金股利	|股票股利	|年均殖利率(%)	|EPS		|盈餘分配率(%)	|
|:---	|---:	|---:	|---:	|---:	|---:	|---:			|---:		|---:			|
|2019	|90.00	|85.00	|87.20	|4.00	|0		|4.59			|4.64		|86.20			|
|2018	|92.70	|83.60	|88.60	|4.00	|0		|4.51			|5.00		|80.00			|
|2017	|95.00	|86.60	|89.50	|3.50	|0		|3.91			|1.40		|250.00			|
|2016	|97.00	|86.20	|91.50	|4.00	|0		|4.37			|4.68		|85.50			|
|2015	|101.00	|77.30	|90.70	|4.00	|0		|4.41			|4.74		|84.40			|

- (9925)新保

|年度 	|股價最高	|股價最低	|股價年均	|現金股利	|股票股利	|年均殖利率(%)	|EPS		|盈餘分配率(%)	|
|:---	|---:	|---:	|---:	|---:	|---:	|---:			|---:		|---:			|
|2019	|39.00	|36.60	|37.90	|2.00	|0		|5.27			|2.18		|91.70			|
|2018	|39.10	|32.80	|37.10	|2.00	|0		|5.38			|2.46		|81.30			|
|2017	|40.90	|37.85	|39.30	|2.00	|0		|5.09			|2.55		|78.40			|
|2016	|42.15	|37.75	|39.90	|2.00	|0		|5.01			|2.81		|71.20			|
|2015	|41.70	|32.60	|39.00	|1.90	|0		|4.87			|2.71		|70.10			|
{% raw %}
<br/>
{% endraw %}
> - 以上 `EPS` 皆為前一年度。
- 我們可以看出近幾年 `新保` 的 `EPS` 開始下降。

## 進一步財報拆解
從上面財報三率中，我們可以發現 `營利率` 是這兩家公司最大的差異。所以我們來進一步的拆解營業費用。
> 營業利益率 = (總產品收入 - 總產品成本 - 營業費用)/ 總收入

### 以 2019/Q2 財報(累計) - 營業利益率 分析比較
{% raw %}
<table>
	<thead>
		<tr>
			<th style="text-align:center"></th>
			<th style="text-align:right">(9917)中保</th>
			<th style="text-align:right">(9925)新保</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align:right">營業收入(%)</td>
			<td style="text-align:right">100.00</td>
			<td style="text-align:right">100.00</td>
		</tr>
		<tr>
			<td style="text-align:right">營業成本(%)</td>
			<td style="text-align:right">63.30</td>
			<td style="text-align:right">67.80</td>
		</tr>
		<tr>
			<td style="text-align:center">營業毛利(%)</td>
			<td style="text-align:right">36.70</td>
			<td style="text-align:right">32.20</td>
		</tr>
		<tr>
			<td style="text-align:right">推銷費用(%)</td>
			<td style="text-align:right">5.85</td>
			<td style="text-align:right">2.93</td>
		</tr>
		<tr>
			<td style="text-align:right">管理費用(%)</td>
			<td style="text-align:right">10.50</td>
			<td style="text-align:right">19.20</td>
		</tr>
		<tr>
			<td style="text-align:right">研究發展費用(%)</td>
			<td style="text-align:right">0.77</td>
			<td style="text-align:right">0.34</td>
		</tr>
		<tr>
			<td style="text-align:right">預期信用減損損失(利益)(%)</td>
			<td style="text-align:right">0.12</td>
			<td style="text-align:right">0.04</td>
		</tr>
		<tr>
			<td style="text-align:center">營業費用(%)</td>
			<td style="text-align:right">17.30</td>
			<td style="text-align:right">22.50</td>
		</tr>
		<tr>
			<td style="text-align:left">營業利益(%)</td>
			<td style="text-align:right">19.40</td>
			<td style="text-align:right">9.77</td>
		</tr>
	</tbody>
</table>
<br/>
{% endraw %}

> - 從上表可以得知，`新保` 在管理費用上比例偏高，而 `管理費用` 包含員工薪資、福利費用...等。
> - 上表只是列出 2019/Q2 財報(累計)，實際上近幾年的財報皆是如此。
> - 上方 `營業利益率` 表格是用比率呈現，如果轉換金額，大家可以發現 `(9925)新保` 的 `管理費用` 金額跟 `(9917)中保` 是很接近的，但是兩間公司所創造的營收金額卻差很多。
> - 從 `104人力銀行` 上登入的員工數，`中保` 是 2600 人，`新保` 是 2000 人，員工數差距 600 卻可以創造出快多一倍的營收，從這邊也可以得知 `中保` 管理方式較有效率。

## 心得
### 2019/08/22
- 保全雙雄當中，`(9917)中保` 雖然股價較高，但相對起來財務較為健全，是可以優先考慮的公司。
- `(9925)新保` 將持續觀察	`管理費用` 是否有改善，如果有改善會是不錯的投資標的。
- 技術面來講兩檔股票經過今年的除權息，目前還是相對低點，參考下圖(資料來源：YAHOO!股市)。
	- (9917)中保
![](/image/20190821-securityGuard/securityGuard-01.jpg)
	- (9925)新保
![](/image/20190821-securityGuard/securityGuard-02.jpg)
- 保全業是內需概念股，不太會因為國際情勢而影響獲利，也由於轉換成本較高，市佔率較高的公司相對較有優勢（保全巡邏相對較有效率），殖利率穩定 `4~5%` 在資產配置中，可以佔有少部分比例以提供穩定現金流來源。

## Reference
[Goodinfo!](https://goodinfo.tw/StockInfo/index.asp)
[YAHOO!股市](https://tw.stock.yahoo.com/)