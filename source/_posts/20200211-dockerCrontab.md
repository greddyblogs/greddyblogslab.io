---
title: 【筆記】在 Docker 中執行 Crontab
date: 2020-02-11 11:31:51
categories:
- Devops
tags: 
- docker
- crontab
---
今天要跟大家介紹，如果我們有個定時任務需要執行，而我們又需要將他包裝在 `Docker` 中，那我們應該如何做呢？

## Quick Start
建議大家閱讀文章前可先參考 [【筆記】Gitlab CI/CD - Python 版本](/2019/02/19/gitlabCicd/)。

### 建立 `root` 檔案
我在我的專案中的 `resources` 資料夾中，新增一個 `cron` 的資料夾，並建立一個 `root` 的檔。
添加要執行的排程至檔案中。
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1939996.js"></script>

> 檔名為 `root` 是因為我們使用系統的 `root` 帳號權限使用，如果是其他帳號，將檔名修改為 `帳號` 名即可。

### 建立 `Dockerfile` 檔案
這邊有兩點要注意：
- `COPY resources/cron/root /var/spool/cron/crontabs/root` 將檔案 COPY 至正確路徑。`root` 為系統帳號名稱。
- `CMD ["crond", "-f", "-d", "8"]` ，需將 `crontab` 結果印出。

<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1939648.js"></script>

### `Pyhton project` 專案目錄結構
![](/image/20200210-pythonCicd/pythonCicd-01.jpg)

## 執行結果
```
[root@iZwz93zqzr3c2q8y93bkm4Z ~]# docker logs -f sb-predict
Using PROFILE = PROD
Connect Database OK
Using PROFILE = PROD
2020 20200208 ORL 94.2412 MIL 117.8744
ORL 94.2412
MIL 117.8744
2020 20200209 TOR 104.8998 BRK 115.6197
TOR 104.8998
BRK 115.6197
2020 20200209 CHO 99.4058 DAL 115.6165
CHO 99.4058
DAL 115.6165
2020 20200209 PHO 113.1644 DEN 108.3133
PHO 113.1644
DEN 108.3133
2020 20200209 MIN 115.7934 LAC 118.4105
MIN 115.7934
LAC 118.4105
2020 20200209 GSW 106.2309 LAL 113.2086
GSW 106.2309
LAL 113.2086
2020 20200209 IND 102.6752 NOP 125.6884
IND 102.6752
NOP 125.6884
2020 20200209 DET 106.1032 NYK 107.8717
DET 106.1032
NYK 107.8717
2020 20200209 SAC 106.5801 SAS 120.2693
SAC 106.5801
SAS 120.2693
```

今天介紹就到這邊喔！如果有問題，請再跟我說喔！