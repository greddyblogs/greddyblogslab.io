---
title: Gitlab CI/CD 教學（一）
date: 2019-02-19 10:15:19
categories:
- Devops
tags: 
- gitlab
- docker
---
Gitlab 提供了許多強大的功能，提供 CI/CD 也提供了 Docker 的 Registry。
今天要教大家在專案中設定 `Dockerfile`，將製作好的 Docker Image 上傳至 Gitlab 中的 Registry。

## Quick Start
今天使用之前寫的專案 [cpay-core_service](https://gitlab.com/cpay2019/cpay-core_service) 當作範例。
我們希望可以將 Gitlab 可以當作自己的 Build Server 這樣就可以不用再去額外的準備其他機器。

### 撰寫 `Dockerfile`
在專案根目錄中，撰寫一個 `Dockerfile` 檔案。
<script src="https://gitlab.com/cpay2019/cpay-core_service/snippets/1825479.js"></script>

### 創建 `.gitlab-ci.yml`
在專案根目錄中，創建一個 `.gitlab-ci.yml` 檔案，下面將對各個區塊進行說明。
<script src="https://gitlab.com/cpay2019/cpay-core_service/snippets/1825640.js"></script>

### 區塊： `cache`
> 我們可以在 Gitlab Server 上，透過 `.gitlab-ci.yml` 創建多個 Docker環境。
由於每個 stages 都可以是被包在不同的 Docker 容器或是共用同一個 Docker 容器。
當分在不同的 Docker 容器上時，執行不同步驟卻需將檔案彼此分享，就可以透過 cache 這個指令撰寫。

範例：
我們是將 `.m2/` 與 `target/*.jar` 進行共享。
```
cache:
  paths:
    - .m2/
    - target/*.jar
```

### 區塊： `stages`
> 決定執行步驟與順序

範例：
`build_jar` -> `build_n_push_image`
```
stages:
  - build_jar
  - build_n_push_image
```

### 區塊： `build_jar`
> 撰寫執行腳本，這邊將進行 build code 與包成 jar file

範例：
- `build_jar` 此名稱可自行定義。
- `stage` 的 `build_jar` 可自行定義，但須對應到 `stages` 中。
- 在此步驟中，我們使用了 DockerHub 提供的 image: `maven:latest`
- 將需要執行的腳本寫在 `script` 中。

```
build_jar:
  image: maven:latest
  stage: build_jar
  script:
    - mvn clean package install
```

### 區塊： `build_n_push_image`
> 撰寫執行腳本，這邊會將會 build 出 Docker Images，並上傳至 Gitlab 的 Registry 中。
另外，我們使用 git commit 的小版本號當作是 tag 值，以利後續的追蹤維護使用。

範例：
- 我們使用了 DockerHub 提供的 image: `gitlab/dind`	 ，此 image 可以在 Docker 中使用 Docker。
- 使用 Gitlab 的 Container Registry，可在專案左側的導覽列進入 `Registry`，內有上傳位置。

![](/image/20190219-gitlabCicd/gitlabCicd-01.jpg)

```
build_n_push_image:
  image: gitlab/dind
  stage: build_n_push_image
  script:
    - docker build -t registry.gitlab.com/cpay2019/cpay-core_service:latest .
    - docker build -t registry.gitlab.com/cpay2019/cpay-core_service:$CI_COMMIT_SHORT_SHA .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker push registry.gitlab.com/cpay2019/cpay-core_service:latest
    - docker push registry.gitlab.com/cpay2019/cpay-core_service:$CI_COMMIT_SHORT_SHA
```

### 驗證
專案左側的導覽列進入 `CI/CD` -> `Pipelines` ，每一次的 commit 都會 trigger pipelines。
![](/image/20190219-gitlabCicd/gitlabCicd-02.jpg)

進入可以看到 Job 的狀態。
![](/image/20190219-gitlabCicd/gitlabCicd-03.jpg)

查看 logs
![](/image/20190219-gitlabCicd/gitlabCicd-04.jpg)

專案左側的導覽列進入 `Registry` ，可以看到 push 的 Docker images。
![](/image/20190219-gitlabCicd/gitlabCicd-05.jpg)