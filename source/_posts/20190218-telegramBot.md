---
title: Telegram Bot設定教學
date: 2019-02-18 17:42:34
categories:
- Other
tags: 
- telegram
---
在軟體工程中，CI/CD 的環節不管是成功失敗，我們都會需要有機器人對我們過版流程進行通知。
今天先在這邊教大家如何使用 [Telegram Bot](https://core.telegram.org/bots) 進行通知。

## Quick Start

### 先加入 Botfather
開啟 Telegram，在搜尋欄位 key： `@Botfather`
![](/image/20190218-telegramBot/telegramBot-01.jpg)

### 依操作步驟取得 HTTP API token
依據 `Botfather` 指示取得 token: `755097180:AAF4tBXa5JhghICwKu7UfbwAGf30a0GsVAA`
![](/image/20190218-telegramBot/telegramBot-02.jpg)

### 加入新創建的 Bot
搜尋欄位 key： `@HelloGreddy2019Bot`
![](/image/20190218-telegramBot/telegramBot-03.jpg)

輸入測試文字 `Hello`
![](/image/20190218-telegramBot/telegramBot-04.jpg)

### 取得 Bot 所在的聊天室 ID
開啟瀏覽器，輸入 `https://api.telegram.org/bot{token}/getUpdates`
如：https://api.telegram.org/bot755097180:AAF4tBXa5JhghICwKu7UfbwAGf30a0GsVAA/getUpdates
取得ID: `647681584`
![](/image/20190218-telegramBot/telegramBot-05.jpg)

### 發送訊息測試
發送訊息
```
curl -X POST "https://api.telegram.org/bot755097180:AAF4tBXa5JhghICwKu7UfbwAGf30a0GsVAA/sendMessage" -d "chat_id=647681584&text=*測試Bot*&parse_mode=Markdown"
```
![](/image/20190218-telegramBot/telegramBot-06.jpg)
驗證
![](/image/20190218-telegramBot/telegramBot-07.jpg)