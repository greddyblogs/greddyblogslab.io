---
title: Vue.js 筆記（一）介紹
date: 2021-05-09 14:37:22
categories:
- vue
tags: 
- vue
---
最近正在學習 `Vue.js` ，接下來會慢慢記錄自己的學習過程。

我們先來看一下[官方網站](https://cn.vuejs.org/)是如何介紹他自己的。
> Vue（讀音/vjuː/，某些視圖）是一套用於託管用戶界面的漸進式框架。與其他大型框架不同的是，Vue被設計為可以自底向上逐層應用。Vue的核心庫只關注視圖層，只需易於上手，又可以與第三方庫或既有項目整合。替換，當與現代化的工具鏈以及各種支持類庫結合使用時，Vue也完全能夠為複雜的單頁應用提供驅動。

與另外兩大前端框架（`Angular`, `React`）的比較

|		|Angular	|React		|Vue		|
|:---:	|:---:		|:---:		|:---:		|
|出生年	|2010		|2013		|2014		|
|開發者	|Google		|Facebook	|Google前工程師|
|語言	|Typescript	|Js			|Js			|
|Github 星星數|73K	|168K		|183K		|
|Library 的大小|大	|中			|小			|
|學習曲線	|難			|難			|易			|

## Quick Start
<!-- md vueRoadMap.md -->

### 安裝
- 開發環境使用
```
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
```
- 生產環境使用
```
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```

### Hello World!
#### Example
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/-/snippets/2117591.js"></script>
#### 說明
- `Vue.js` 的核心是一個允許採用簡潔的模板語法來聲明式地將數據渲染進 DOM 的系統，上面範例我們將需要進行 `Vue` 渲染的地方進行宣告，宣告範圍為 `#app`。
- 將 `data` 中的 `message` 的值，渲染至頁面中。

#### 畫面
![](/image/20210509-vue/vue-01.jpg)

#### 進一步修改值
如果我們在 console 中，修改 `app.message` 的值，畫面中的值也會同步被修改，這就是 `Vue` 強大的雙向綁定。
![](/image/20210509-vue/vue-02.jpg)

今天的筆記就到這邊，期待下次再會！
## Reference
[官方網站](https://cn.vuejs.org/)