---
title: Websocket 介紹
date: 2019-10-30 23:42:17
categories:
- Java
tags: 
- websocket
---
今天要來跟大家介紹一下 `Websocket` 的使用場景與方式。
`Websocket` 是一個網路協定，可以用來進行雙向（Client 與 Server）的資料交換。
主要的應用場景如，當網頁需要進行高頻率的實時更新時，如果使用輪詢所消耗的開銷（如：交握、認證...等）將會大得多。

接下來我們將會在 `spring boot` 的框架下加入 `Websocket` 的服務，我們開始吧！

## Quick Start
我們接下來會針對 Server 端與 Client 端分別進行範例說明
### Server 端
我們將會在 `spring boot`下添加 `websocket`
- 在 `build.gradle` 添加依賴

```
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-websocket'
}
```
- 實作 `WebSocketConfigurer` 且 `EnableWebSocket`，新增 `WebSocketConfig.java`

```
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new GpWebsocketHandler(), "/webSocket/{INFO}")
                .setAllowedOrigins("*");
    }
}
```

- 實作 `WebSocketHandler`，新增 `GpWebsocketHandler.java`
  - 新增了一個 static 的 map，用來儲存建立上連線的用戶端 session，當用戶端連上線時，會進行添加，斷線或是離線時，會進行移除。
  - 因 Server 端可提供多種推播資訊，我們將使用 `command` 進行區分，因此我新增一個 `WebsocketCommandEnum`
```
public enum WebSocketCommandEnum {

    GAMEBOARDCAST("遊戲廣播");

    @Getter
    private String cname;

    WebSocketCommandEnum(String cname) {
        this.cname = cname;
    }
}
```
  - 另外，實作了一個 `pushMessageBySocketCommand` 的 public 方法，當有資料需要進行更新時，可以呼叫此方法進行訊息推播。

<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1932388.js"></script>

### Client 端
我們使用的是 `H5` 的 `websocket`，我們直接來看範例
```
var ws = new WebSocket("ws://localhost:8080/gp-console/webSocket/INFO={\"command\":\"GAMEBOARDCAST\"}");
ws.onmessage = WSonMessage;
var cntMap = new Map();
function WSonMessage(event) {
    var data = event.data;
    // TODO 資料的處理(如：更新畫面欄位)
};
```

## 補充說明
我們可以在連線的過程中透過添加攔截器，進行身份的認證，添加的方式如下：
```
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new GpWebsocketHandler(), "/webSocket/{INFO}")
        		.addInterceptors(new MyInterceptor())
                .setAllowedOrigins("*");
    }
}
```
需要實作 `HandshakeInterceptor`

今天先到這邊了，如果有其他問題，歡迎寄信討論！謝謝！