---
title: 【筆記】Gitlab CI/CD - Python 版本
date: 2020-02-10 16:29:23
categories:
- Devops
tags: 
- docker
- gitlab
- ci/cd
- python
---
今天要跟大家分享一下，`Python` 的專案如何透過 `Gitlab` 做到 `CI/CD` 呢？

大部分的東西其實跟 `Java` 的版本沒有太大的差異，大家可以參考 [Gitlab CI/CD 教學（一）](/2019/02/19/gitlabCicd/)。

我這邊特別挑幾個需要注意的地方出來。

## Quick Start
我們這次要特別注意的有 `Python` 的 `package` 的安裝與環境變數的設定，接下來我開始說明吧！

### Dockerfile 撰寫
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1939648.js"></script>
我這邊使用的原始 `image` 是 `python:3.7.2-alpine3.9`。有以下幾點要注意。
- 先將 `apk` 做更新，並且加入 `tzdata`，我們希望 `docker` 內部時區與 `Host` 時區一至，可參考[【筆記】Docker Timezone 設定](/2020/01/31/dockerTimeZone/)。
- 設定工作目錄，並將程式碼複製過去。
- 安裝所需要的 `Python package`，不過在安裝的時候，有時候會出現 `gcc` 之類的錯誤，主要是因為我們選用的是 `alpine` 是比較精簡的 `Linux`，因此需要多加上 `apk add --no-cache g++ libressl-dev postgresql-dev libffi-dev gcc musl-dev python3-dev && \`
- 設定 `PROFILE` 參數，希望依據不同環境進行不同參數切換。
- 設定 `PYTHONPATH` 參數。
- 使用 `crontab` 來定期執行我的 `Python`。

### `.gitlab-ci.yml` 撰寫
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1939650.js"></script>
我們使用的 `image` 是 `gitlab/dind`。
- 我這邊使用到了 `ssh key` 的設定，大家可以參考 [Gitlab CI/CD 教學（二）](/2019/02/20/gitlabCicd2/)。
- 設定與 `Host` 主機相同時區，大家可以參考[【筆記】Docker Timezone 設定](/2020/01/31/dockerTimeZone/)。
- 設定了 `network`，大家可以參考[【筆記】Docker Network - Bridge Mode](/2020/01/27/dockerNetwork/)。

### `Pyhton project` 專案目錄結構
![](/image/20200210-pythonCicd/pythonCicd-01.jpg)

今天介紹就到這邊喔！如果有問題，請再跟我說喔！