---
title: 【筆記】Docker Keepalive作法
date: 2020-07-21 16:18:35
categories:
- Devops
tags: 
- docker
---
今天要跟大家分享我遇到的一個簡單的小問題，最近想要在啟動 `docker` 之後之行一個 `shell script`，不過遇到了當執行完了 `shell script` 後 `docker` 的 `container` 就自動被關掉了。如果我今天想要持續使這個 `container` 活著，我應該怎麼做呢?

## Quick Start
- Dockerfile
<script src='https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1997518.js'></script>
- docker-entrypoint.sh
<script src='https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1997521.js'></script>
> 處理方式就是在 `shell script` 最後加上 `tail -f /dev/null` 即可。

今天的筆記就先簡單紀錄，之後有需要再補上更詳細內容。