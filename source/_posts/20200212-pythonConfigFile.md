---
title: 【筆記】在 Python 中使用 configparser Package
date: 2020-02-12 23:52:28
categories:
- Devops
tags: 
- python
- configparser
- docker
- ci/cd
---
今天想要跟大家介紹，當我們用 `Python` 時，如果我們想要依據不同環境切換不同的參數設定，我們可以使用 `configparser Package`。
如果我們想要更進階的結合 `docker` 我們應該如何做了？

## Quick Start
建議大家閱讀文章前可先參考 [【筆記】Gitlab CI/CD - Python 版本](/2019/02/19/gitlabCicd/)。

### 建立 application.ini 檔案
將需要參數化的參數定義在 `application.ini` 中，我們這邊定義兩組參數，分別是 `DEFAULT` 與 `PROD`。
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1940840.js"></script>

### 建立 sbPredict.py 檔案
這邊有幾個需要注意的地方：
- 我需要先決定我使用的 `PROFILE` 值為何？ `PROFILE` 的值從環境變數來。
- 在抓取 `application.ini` 檔案時，如果只有寫 `file_path = '../resources/application.ini')` 是不行的，因為他並不是由當前路徑去尋找檔案，所以我調整成完整路徑。

<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1940842.js"></script>

### 建立 dockerfile
大家可以看到我這邊有增加一個環境變數 `PROFILE`，完整如下：
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1939648.js"></script>

### docker 的啟動指令
我們可以決定我們需要使用哪一組的環境參數帶入不同的 `PROFILE` 值。
```
docker run --name sb-predict -d -h sb-predict -e PROFILE=PROD --network sb-net -v /etc/localtime:/etc/localtime:ro registry.gitlab.com/sportsbook1/sb-predict:$CI_COMMIT_SHORT_SHA
```

### `Pyhton project` 專案目錄結構
![](/image/20200210-pythonCicd/pythonCicd-01.jpg)

### 補充說明
如果是使用 `PyCharm IDE` 要如何新增環境變數呢？可以參考下圖。 
![](/image/20200212-pythonConfigFile/pythonConfigFile-01.jpg)

今天介紹就到這邊喔！如果有問題，請再跟我說喔！