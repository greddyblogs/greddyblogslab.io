---
title: 【專案】Fake Flyway
date: 2020-07-23 13:53:48
categories:
- Devops
tags: 
- flyway
---
最近想要導入 `flyway` 功能來做資料庫腳本的版本控管，但現行的資料庫是 `mysql5.6` 而 `flyway` 免費版本並不支援該版本，所以只好依照 `flyway` 的概念簡單做了一個出來。

大家可以參考我實作的原始碼 [Fake Flyway](https://gitlab.com/greddywork/flyway)。

補充說明: 大家可以看一下官網上 `flyway` 是怎麼介紹他自己的。
> Version control for your database.
Robust schema evolution across all your environments.
With ease, pleasure and plain SQL.

## Quick Start
這邊會介紹
- 如何使用
- 設計理念
- 未來展望

### 如何使用
- 開始可以先使用 `Docker` 啟動 `mysql` 資料庫

```
docker run --name mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306 -d mysql
```

- 修改 `Config`

```
{
  "db": {
    "url": "localhost",
    "username": "root",
    "password": "123456"
  }
}

```

- 將要執行的腳本放在 `sql` 的資料夾中
如: `2020070801_Greddy_sample_create_database.sql`

```
CREATE DATABASE test DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
> 這邊有個地方要注意的就是檔案命名規則為: `日期+流水序號` + `作者` + `描述`

### 設計理念

#### 說明
在一開始的時候會先建立一個 `schema` 為 `db_migrate`，並且建立一張表 `migration`。
每次執行時，會先將 `sql` 資料夾的檔案與資料表 `migration` 進行比對，看看是否有執行過。
如果沒有執行過就會進行執行，並且寫入。

#### 流程圖
<iframe src="https://viewer.diagrams.net/?title=Fake%20Flyway#R7VrbVuM2FP0aP5IV353HSbhMp5RFCy3lqUuxFVtFtoKskISvrxTLVwXHSRwoM8MDWMfHR5b2PjcZzZzEqysK5tFvJIBYM4bBSjPPNcMYeTr%2FLQTrTGAbTiYIKQoykV4K7tArlMKhlC5QANOaIiMEMzSvC32SJNBnNRmglCzrajOC67POQQgVwZ0PsCp9QAGLMqlnuKX8K0RhlM%2BsO6PsTgxyZbmSNAIBWVZE5oVmTighLLuKVxOIxd7l%2B%2FLwy%2FoBXz85V99%2BT5%2FBn%2BNf72%2F%2BOsuMXe7zSLEEChPWr2kjM%2F0C8ELul1wrW%2BcbSMkiCaAwMtTMccRizC91fvkvZGwtAQcLRriIUBaRkCQAXxMyl3ozkjCpposxTIIvAlg%2BnmLiP2WiS4SxnIOPpL7HRymj5KnAThgogBDKGEwhHgP%2FKdy86IRgQvmthCRQmAo4GeRaype7KKXjjnsrMUjJgvqwRc%2BUFAc0hG32pBOJ96vwVCJ3BUkMGV1zBQoxYOilTmYgfSIs9Erc%2BYWEfg8amAoN7vgKWAsXBA7LCDF4NwebDVny6FHnRxV3vrRxiEGaStR2gLofKC%2BQMrhq3cb8riV9WQYzTw6XZWTQHSmLKlEhf6z3jbeUjX%2FkwfIHc0EOMl3%2FLYwN7Hz4WLwIH5yvaqP1QSzZ6bqO6rrP9xOd3aTfoHN%2Fw8xb8Mcru5BhdrfrSo7l6aOzJ0tLtwTxZZUqZDZLIauH%2BFxH0ruYK5961OBttjb5VIO6xZsfzmZbYfMNUci8mw%2Bfmu6nJ%2BVWPXc%2FUuonI2VfXHIULgXTf2IUUsDgIPuLSKIZDubbMp5SfhWKK5SKbVihlPFXvFRDaUTi6SLdnb1qkAtCXYIYYbF9XyF%2BgQz5YEuOAxiFCR%2F4HH1It3OJT4mSkI%2BccnS%2F4S5PB%2B%2BX%2B3S7Y%2FJrBpHekp%2BrQOxTyOHVRBhr4pr6EYyBeI0k2DjElEP6vdUnpm3VMXJtFSPTOU2B0pbsKhhxiAQAAor0WTw%2FQwKJIUpKCW%2FSOPs%2FFh1l17sC1tmDDPcdy8et6KjtWwRE%2FEMiRk8hTDaxEPoLJrb980bD02NpWh2x9E6FpdqDtTjQJyyMOvQBK8QqbQAfPcppxHXZBIjBWqt0BAf3Do1y6bC6rS1u7mwmjO0sPbQuyxsBfTRwbMvyDNf2HGPo1rluNjicrUZpC1SzbsNp7G79BecYWFfU5kIhPbJq3LqdtuIz%2B7YbW%2F1mi39VvO8Q3p6af8Y78e%2BokOf9DHnHhDy9FvDK%2BHf6kNdWznc7P%2Bk95Fl6vXTmRddgWPlpGOwa8hSzbqvZDw%2BAo58uVbjUyDUqTjUcWK7bsZbYz7GEkVtIEcdOVLn%2Fq%2FiuH%2FtdYbtbmI5SCfThbYpZr19ve6OsGdnNWRuumVns1VF1tXfbdvQvGHQtPKLuqkpnJTok3oThL%2FJGjIIgq25gil43hyUZMeVmcOP2WLPP21os%2BUlWPqwVp8tVEr8dhN5svYYD08iRPJKGZ7o5cOoB%2Bkxv1KS9HFG2%2BFYNQtFsNw5GvrNDkOYRVeGyH3ZEpasf0X7cnHdI5%2FxhZeRRme00ZaTROBeyzF4Sm2LWeZfE1vgeaFnD1rfUh8fp558KTps4O39l%2FHR5M4tkb4beM545Tb1%2BlpO7y7GJ1BxYXt2wd3wa5cPy36My9fJ%2FzMyL%2FwA%3D" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### 未來展望
- 支援更多種 DB 類型
- 對檔案進行 checksum

## Reference
[Flyway](https://flywaydb.org/)

今天就介紹到這邊，如有其他疑問請再跟我說。