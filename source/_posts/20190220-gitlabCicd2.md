---
title: Gitlab CI/CD 教學（二）
date: 2019-02-20 10:15:19
categories:
- Devops
tags: 
- gitlab
- docker
- aws
---
接續上次[Gitlab CI/CD 教學(一)](/2019/02/19/gitlabCicd/)，今天要跟大家說說要如透過 Gitlab 的 CI/CD 將程式部署到 AWS Server 上。

## Quick Start
在正式開始之前，會需要大家先去申請[AWS Server](https://aws.amazon.com/tw/?nc2=h_lg)，並先建立一個 EC Server，有關 AWS 相關文章，後續有空再補上給大家。

### ssh 登入 AWS Server 主機
- 透過 ssh 登入 AWS Server 主機

```
ssh ec2-user@52.221.216.202 -i ./cpay.pem
```
![](/image/20190220-gitlabCicd2/gitlabCicd2-01.jpg)

### 安裝 Docker on Amazon EC2
- 更新 yum

```
sudo yum update -y
```
![](/image/20190220-gitlabCicd2/gitlabCicd2-02.jpg)

- 這邊是使用 Amazon Linux 2，指令如下

```
sudo amazon-linux-extras install docker
```
![](/image/20190220-gitlabCicd2/gitlabCicd2-03.jpg)

- 啟動 Docker 服務

```
sudo service docker start
```
![](/image/20190220-gitlabCicd2/gitlabCicd2-04.jpg)

- 將 ec2-user 加入可執行 Docker 的 group

```
sudo usermod -a -G docker ec2-user
```
![](/image/20190220-gitlabCicd2/gitlabCicd2-05.jpg)

- 登出後重新登入，驗證功能

```
docker ps
```
![](/image/20190220-gitlabCicd2/gitlabCicd2-06.jpg)

### 設定 Gitlab
- Gitlab 專案左側的導覽列進入 `Setting` -> `CI/CD` -> `Environment variables`。將 ssh 的 private key 填入。

![](/image/20190220-gitlabCicd2/gitlabCicd2-07.jpg)

- 修改`.gitlab-ci.yml`

`AWS_SSH_KEY` 為上面設定的參數名稱。

```
deploy:
  stage: deploy
  before_script:
    - mkdir -p ~/.ssh
    - echo -e "$AWS_SSH_KEY" > ~/.ssh/id_rsa  
    - chmod 600 ~/.ssh/id_rsa
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh ec2-user@52.221.216.202 -- "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY && docker pull registry.gitlab.com/cpay2019/cpay-core_service:$CI_COMMIT_SHORT_SHA && docker run --name cpayCoreService -d -h cpayCoreService -p 8080:8080 registry.gitlab.com/cpay2019/cpay-core_service:$CI_COMMIT_SHORT_SHA"
```

完整 script 請參考下方。
<script src="https://gitlab.com/cpay2019/cpay-core_service/snippets/1826887.js"></script>

- 驗證

確認 pipeline 執行成功
![](/image/20190220-gitlabCicd2/gitlabCicd2-08.jpg)

登入 AWS Server 確認
![](/image/20190220-gitlabCicd2/gitlabCicd2-09.jpg)

### 問題：當重複布版的時候，會出現 container 已經存在

需加入清除 Docker container 與 images 的相關指令在腳本中
```
docker ps --filter "name=cpayCoreService" -a -q | xargs --no-run-if-empty docker rm -f
docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi
```

完整範例請參考下方。
<script src="https://gitlab.com/cpay2019/cpay-core_service/snippets/1826928.js"></script>

### 補充
大家可以參考之前的文章[Telegram Bot設定教學](/2019/02/18/telegramBot/)，試著在部署前後都進行通知，讓整個 CI/CD 機制更加完善。


## Reference
[To install Docker on an Amazon EC2 instance](https://docs.aws.amazon.com/en_us/AmazonECS/latest/developerguide/docker-basics.html)