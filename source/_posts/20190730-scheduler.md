---
title: Spring Boot 中使用 Scheduler
date: 2019-07-30 10:53:24
categories:
- Java
tags: 
- scheduler
---
今天要跟大家介紹如何在 Spring Boot 的專案中，啟動一個排程任務時，最簡單方便的首選就是 `Scheduler`

## Quick Start

首先我們需要在 Spring Boot 啟動的 `Main` 程式上，加上 `@EnableScheduling`
```
@SpringBootApplication
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
```

我們來寫一個 TestJob.java
```
@Component
public class TestJob {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(cron = "0/5 * * * * ?")
    private void testJob() {
        System.out.println("現在時間：" + dateFormat.format(new Date()) + " - Hello World!");
    }
}
```

執行看輸出：
![](/image/20190730-scheduler/scheduler-01.jpg)

我們可以從上面發現，我們的輸出是每5秒執行一次。主要的寫法是使用 `cron` 的表達式
```
@Scheduled(cron = "0/5 * * * * ?")
```

而 `cron` 的表達式的寫法可以參考[wiki](https://en.wikipedia.org/wiki/Cron)

如果有其他問題，歡迎寄信討論！謝謝！

## Reference
[Cron](https://en.wikipedia.org/wiki/Cron)
