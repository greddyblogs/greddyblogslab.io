---
title: 【筆記】Domain Check 作法
date: 2020-05-01 17:51:32
categories:
- Java
---
最近收到一個任務，我們需要先去判別 `Domain` 是否還存活著，如果存活就正常的去發送 `API` 否則就停止。

判斷的作法如下：
- 增加 `timeout` 設定

<script src='https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1972327.js'></script>

今天的筆記就先簡單紀錄，之後有需要再補上更詳細內容。