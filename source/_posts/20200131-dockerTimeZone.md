---
title: 【筆記】Docker Timezone 設定
date: 2020-01-31 10:57:29
categories:
- Devops
tags: 
- docker
- timezone
---
如果我們要在 `Docker` 內部設定與 `host` 相同的時區，該如何設置呢？

## Quick Start
網路上很多人都說，在啟動 `container` 時可以添加以下指令：
- 方法一：
> -v /etc/localtime:/etc/localtime:ro
- 方法二：
> -e "TZ=Asia/Taipei"

不過有的時候可以，有的時候卻沒有作用，是為什麼呢？原是是取決於你使用的 `docker image`

### 使用 `alpine` 的版本

#### dockerfile
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1935075.js"></script>

- 我們在 `dockerfile` 中，加入安裝 `tzdata` 的指令

```
RUN apk update && \
    apk add -U tzdata
```

#### 啟動指令

- 添加 `-v /etc/localtime:/etc/localtime:ro`

```
docker run --name sb-crawler -d -h sb-crawler -p 8081:8081 -e SPRING_PROFILES_ACTIVE=prod -v /home/ec2-user/log/sb-crawler:/log -v /etc/localtime:/etc/localtime:ro registry.gitlab.com/sportsbook1/sb-crawler:$CI_COMMIT_SHORT_SHA
```

### 使用非 `alpine` 的版本
缺點是體積比 `alpine` 的版本大非常多，也會花費較多的時間(push image)

#### dockerfile
- 使用的 `image` 為 `openjdk:12`

#### 啟動指令
- 添加 `-e "TZ=Asia/Taipei"`

今天筆記記錄到這邊，有問題再跟我說喔！
