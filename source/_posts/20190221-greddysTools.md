---
title: Greddy 都使用哪些 Tools 呢？
date: 2019-02-21 16:13:08
categories:
- Other
tags: 
- mac
---
今天想要跟大家分享一下平常 Greddy 的 Mac 電腦都使用哪些的 Tools 呢？若大家有好的 Tools 也歡迎分享給我知道喔！

## Quick Start
下面會介紹我平常工作上使用到的 Tools，今天只是簡單的介紹，若有需要詳細的 Tools 用法，我再開文章與大家分享囉。

- ***IDE***
  - [IntelliJ IDEA](https://www.jetbrains.com/idea/) :
  以前 Greddy 也是使用 Eclipse，不過現在也轉入 IDEA 的懷抱中囉！工具不分好壞，只有習慣與否。IDEA 有免費的社群版與付費的專家版。基本上一般開發免費的社群版是已經夠用囉！
  - [Sublime Text](https://www.sublimetext.com/) : 
  相當強大的編輯器。
  - [MacDown](https://macdown.uranusjr.com/) : 
  好用的 Markdown 編輯器。

- ***Tools***
  - [Sequel Pro](https://www.sequelpro.com/):
  Mysql 資料庫連線工具。
  - [Balsamiq Mockups 3](https://balsamiq.com/):
  UI Design / Wireframes 工具，需付費。
  - [Draw.io](https://www.draw.io/):
  畫流程圖的線上工具。
  - [DbSchema](https://www.dbschema.com/):
  資料庫管理工具，需付費。
  - [Docker](https://www.docker.com/):
  Docker Container。
  - [Postman](https://www.getpostman.com/):
  測試 API 的最佳利器。
  - [JD-GUI](http://jd.benow.ca/)
  Java反編譯工具。
  - [Sourcetree](https://www.sourcetreeapp.com/):
  Git GUI 工具。

- ***OTH***
  - [Evernote](https://evernote.com/intl/zh-tw):
  雲端筆記軟體。
  - [Snappy](http://snappy-app.com/):
  好用的截圖軟體。
  - [iTerm2](https://www.iterm2.com/):
  好用且漂亮的 terminal 工具，可搭配 [zsh theme](https://ohmyz.sh/) 打造屬於自己的視覺風格。

以上這些 Tools 基本上是我拿到一台新電腦一定會先裝的工具喔！這邊分享給大家。