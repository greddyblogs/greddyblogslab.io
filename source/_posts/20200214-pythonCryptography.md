---
title: 【筆記】在 Python 中使用 cryptography Package
date: 2020-02-14 11:56:19
categories:
- Python
tags: 
- python
- cryptography
---
參考上一篇 [【筆記】在 Python 中使用 configparser Package](/2020/02/14/pythonConfigFile/)，我們在 `Config` 檔案中會出現敏感性資訊，如 DB 的密碼，我們不應該將這些敏感資訊暴露在檔案中，需要做過簡單的加密。
如果是 `Java` 的專案，可以參考之前寫的 [Jasypt 教學](/2019/02/27/jasypt/)，今天我們要來介紹的是 `Python` 專案中，處理的方式。

我們使用的是 `cryptography` Packag，讓我們開始吧！

## Quick Start

### 建立 encryptUtil.py
我寫了三個 `function`，分別是產生 key, 加密與解密。
最下方將需要加密的資料填入產生字串，如範例中的 `abc123`。
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1941280.js"></script>

> 提醒大家，產生完之後密碼記得拿掉，不要 commit 至 Server 上。

### 執行 encryptUtil.py
執行的結果如下：
```
jSb0RFUnStj0BtfDArzIVwiSTJGyacfPwWFRgM7PrDY=
gAAAAABeRh48pdThNvjd0XhHgOsY8_t3pzxJytZXxPT6fNSpXZUbI3iXefAIUAzBytwGnFyTDg7igeRDFXyoe6shAr27cTGI7Q==
abc123
```

### 調整 application.ini
結合上篇的[【筆記】在 Python 中使用 configparser Package](/2020/02/14/pythonConfigFile/)，我們進行調整。
- 我們增加一個 `enc_key` 的參數。
- 我們將 `mysql_password` 的值取代成上面所產生的加密後字串。
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1941288.js"></script>

### 調整 sbPredict.py
調整 `get_conn()`，先取得 `enc_key`，再進行解密。
<script src="https://gitlab.com/greddyblogs/greddyblogs.gitlab.io/snippets/1941289.js"></script>

### `Pyhton project` 專案目錄結構
![](/image/20200210-pythonCicd/pythonCicd-01.jpg)

今天介紹就到這邊喔！如果有問題，請再跟我說喔！