---
title: 【筆記】Docker Network - Bridge Mode
date: 2019-11-27 16:43:38
categories:
- Devops
tags: 
- docker
- network
- bridge
---
今天想要跟大家分享一個我自己還是 Docker 新手時常常遇見的問題。
我以前常常沒注意每一個 Docker 的 container 是一個獨立的系統，如果是微服務架構，兩個服務彼此需要互連時，發生連線不到的問題。

處理這問題的方法很多，這個是整個系統的網路架構上的設計，包含
- 服務是否需要拆分在不同機器、不同網段甚至是個外網的服務
- DNS、Proxy 或是 Hosts...等解決方式。
- 其他

今天不是要跟大家討論整個系統的網路架構。
今天我們先把問題簡化成，當我在同一台機器上，起多個 Container 時，我們要如何讓 Container 彼此互連呢？

Docker Network 有以下幾種模式
- Bridge Mode (Default)
- Host Mode
- None Mode
- Container Mode

我們今天要來介紹的就是 `Bridge Mode`

## Quick Start

- 我們先來查看一下我們機器上目前 Docker 上的 network 設定吧！
```
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
bf401e84049f        bridge              bridge              local
bbec29c80c3f        host                host                local
5fe810c87ea2        none                null                local
```
> 我們可以發現 default 就會有三個，分別是 `bridge` 、 `host` 與 `none`。

- 我們透過 `docker-compose` 啟動 `mysql`
```
$ docker-compose -f docker-compose-mysql.yml up -d
Creating network "mysql_default" with the default driver
Creating gp-mysql ... done

$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
bf401e84049f        bridge              bridge              local
bbec29c80c3f        host                host                local
07381e33f02b        mysql_default       bridge              local
5fe810c87ea2        none                null                local
```
> 多了一個 `mysql_default` 的 bridge 模式

- 使用 `inspect` 查看明細
```
$ docker network inspect mysql_default
[
    {
        "Name": "mysql_default",
        "Id": "07381e33f02bfb64b4cce071b82a0f36a262ef0e2d2b83ca98a44b29c8da23d9",
        "Created": "2019-11-27T09:16:24.1418653Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": true,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "1a02b897231c8e26380cb430288952185336492735900a6ce1760e31424b2cd2": {
                "Name": "gp-mysql",
                "EndpointID": "e80517371e315d4e4c5a826ae66eb4787dead2199636b78dd291b0f3cc784b82",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {
            "com.docker.compose.network": "default",
            "com.docker.compose.project": "mysql",
            "com.docker.compose.version": "1.24.1"
        }
    }
]
```

- 啟動另一個 container `docker run` 的指令多一個 `network` 參數，如
```
$ docker run --name gp-lotcenter -d -e JAVA_OPTS='-Xms1g -Xmx1g' --network mysql_default -p 8003:8003 -p 6565:6565 gp-lotcenter
```
> 使用連線資訊時，可以用 container name 取代 ip
```
spring:
  datasource:
    url: jdbc:mysql://{container name}:3306/gp_lotcenter?useSSL=false&characterEncoding=UTF-8
```

- 建立一個新的 `network`
```
$ docker network create --driver bridge gp-group
```

- 移除一個 `network`
```
$ docker network rm gp-group
```

今天的筆記先記錄到這邊，之後有更多的了解再補上。
如果有其他問題，歡迎寄信討論。