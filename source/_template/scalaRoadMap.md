接下來將要分享我的`Scala`學習筆記，有任何建議與需要改進的地方，也請大家多多指教。
>參考書籍[Learning Scala](https://www.oreilly.com/library/view/learning-scala/9781449368814/)

- [Scala教學（一）介紹與安裝](/2019/07/18/scala/)
- [Scala教學（二）常數、參數與型態介紹](/2019/07/19/scala2/)
- [Scala教學（三）表達式與條件語句](/2019/07/20/scala3/)
- [Scala教學（四）函式](/2019/07/22/scala4/)
- [Scala教學（五）進階函式](/2019/07/22/scala5/)
- [Scala教學（六）集合](/2019/07/25/scala6/)