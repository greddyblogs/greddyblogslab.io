jQuery(document).ready(function(){
	var QRBox=$('#QRBox');
	var MainBox=$('#MainBox');
	var AliPayQR='/image/alipayQrcode.jpg';
	var WeChanQR='/image/wechatpayQrcode.jpg';
	var JkoPay='/image/jkopayQrcode.jpg'

	function showQR(QR){
		if(QR){
			MainBox.css('background-image','url('+QR+')');
		}
		$('#DonateText,#donateBox,#github').addClass('blur');
		QRBox.fadeIn(300,function(argument){
			MainBox.addClass('showQR');
		});
	}

	$('#donateBox>li').click(function(event){
		var thisID=$(this).attr('id');
		if(thisID==='AliPay') {
			showQR(AliPayQR);
		} else if(thisID==='WeChat') {
			showQR(WeChanQR);
		} else if(thisID==='JkoPay') {
			showQR(JkoPay);
		}
	});

	MainBox.click(function(event){
		MainBox.removeClass('showQR').addClass('hideQR');
		setTimeout(function(a){
			QRBox.fadeOut(300,function(argument){
				MainBox.removeClass('hideQR');
			});
			$('#DonateText,#donateBox,#github').removeClass('blur');
		},600);
	});
});